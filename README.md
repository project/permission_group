# CONTENTS OF THIS FILE

* Introduction
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* Maintainers

# INTRODUCTION

Provides the ability to group permissions into a new permission.

* For a full description of the module, visit the project page:
  https://drupal.org/project/permission_group

* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/permission_group

# RECOMMENDED MODULES

* No extra module is required.

# INSTALLATION

* Install as usual, see
  https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
  information.

# CONFIGURATION

* Create and configure permission groups at `admin/people/permission_groups`
* Then the permission groups can be assigned to roles via `admin/people/permissions`
or `admin/people/permission_groups/assign_to_role`

# TROUBLESHOOTING

No known issues at the moment

# MAINTAINERS

Current maintainers:
* Alex Pott (https://drupal.org/user/53892)
