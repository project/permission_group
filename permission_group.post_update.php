<?php

/**
 * @file
 * Post update functions for Permission Group module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\permission_group\Entity\PermissionGroup;
use Drupal\user\Entity\Role;

/**
 * Remove unnecessary dependencies on permission_group module.
 */
function permission_group_post_update_fix_dependencies(&$sandbox = NULL) {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'user_role', function (Role $role) {
    $dependencies = $role->getDependencies();
    // Is there a dependency on the permission group module.
    if (isset($dependencies['module']) && in_array('permission_group', $dependencies['module'], TRUE)) {
      // Re-save the role if there are no linked groups.
      if (empty($role->getThirdPartySetting('permission_group', 'groups', []))) {
        $role->unsetThirdPartySetting('permission_group', 'groups');
        return TRUE;
      }
    }
    return FALSE;
  });
}

/**
 * Calculate permission_group dependencies and remove non-existent permissions.
 */
function permission_group_post_update_update_roles(&$sandbox = NULL) {
  $cleaned_groups = [];
  $existing_permissions = array_keys(\Drupal::service('user.permissions')->getPermissions());
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'permission_group', function (PermissionGroup $group) use ($existing_permissions, &$cleaned_groups) {
    $removed_permissions = array_diff($group->permissions(), $existing_permissions);
    if (!empty($removed_permissions)) {
      $cleaned_groups[] = $group->label();
      \Drupal::logger('update')->notice(
        'The permission %group has had the following non-existent permission(s) removed: %permissions.',
        [
          '%group' => $group->label(),
          '%permissions' => implode(', ', $removed_permissions),
        ]
      );
    }
    $permissions = array_intersect($group->permissions(), $existing_permissions);
    $group->set('permissions', $permissions);
    return TRUE;
  });

  if (!empty($cleaned_groups)) {
    return new PluralTranslatableMarkup(
      count($cleaned_groups),
      'The permission group %group_list has had non-existent permissions removed. Check the logs for details.',
      'The permission groups %group_list have had non-existent permissions removed. Check the logs for details.',
      ['%group_list' => implode(', ', $cleaned_groups)]
    );
  }
}
