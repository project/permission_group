<?php

namespace Drupal\permission_group\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\permission_group\Entity\PermissionGroup;

/**
 * Provides the permissions administration form for a specific permission group.
 *
 * @internal
 */
class PermissionGroupPermissionsSpecificForm extends PermissionGroupPermissionsForm {

  /**
   * The specific permission group for this form.
   *
   * @var \Drupal\permission_group\Entity\PermissionGroup
   */
  protected $permissionGroup;

  /**
   * {@inheritdoc}
   */
  protected function getGroups() : array {
    return [$this->permissionGroup->id() => $this->permissionGroup];
  }

  /**
   * Builds the user permissions administration form for a specific role.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\permission_group\Entity\PermissionGroup|null $permission_group
   *   (optional) The permission group used for this form. Defaults to NULL.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, PermissionGroup $permission_group = NULL) : array {
    $this->permissionGroup = $permission_group;
    return parent::buildForm($form, $form_state);
  }

}
