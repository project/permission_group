<?php

namespace Drupal\permission_group\Form;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the group permissions administration form.
 *
 * @internal
 */
class PermissionGroupPermissionsForm extends FormBase {

  /**
   * String used in form parents to prevent clashes with config entity IDs.
   */
  const LINKED_GROUP_PARENT = 'linked_group$';

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The permission group storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $storage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new UserPermissionsForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage
   *   The permission group storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, ConfigEntityStorageInterface $storage, ModuleHandlerInterface $module_handler) {
    $this->permissionHandler = $permission_handler;
    $this->storage = $storage;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : PermissionGroupPermissionsForm {
    return new static(
      $container->get('user.permissions'),
      $container->get('entity_type.manager')->getStorage('permission_group'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'permission_group_permissions';
  }

  /**
   * Gets the permission groups to display in this form.
   *
   * @return \Drupal\permission_group\Entity\PermissionGroup[]
   *   An array of permission group objects.
   */
  protected function getGroups() : array {
    /** @var \Drupal\permission_group\Entity\PermissionGroup[] $groups */
    $groups = $this->storage->loadMultiple();
    return $groups;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    $groups = $this->getGroups();
    // Store permission group IDs for use when saving the data.
    $form['group_ids'] = [
      '#type' => 'value',
      '#value' => array_keys($groups),
    ];

    // Render group/permission overview:
    $hide_descriptions = system_admin_compact_mode();
    $permissions = $this->permissionHandler->getPermissions();

    $form['system_compact_link'] = [
      '#id' => FALSE,
      '#type' => 'system_compact_link',
    ];

    $form['permissions'] = [
      '#type' => 'table',
      '#header' => [$this->t('Permission')],
      '#id' => 'permissions',
      '#attributes' => ['class' => ['permissions', 'js-permissions']],
      '#sticky' => TRUE,
    ];
    foreach ($groups as $group) {
      $form['permissions']['#header'][] = [
        'data' => $group->label(),
        'class' => ['checkbox'],
      ];
    }

    /** @var \Drupal\permission_group\Entity\PermissionGroup[] $all_groups */
    $all_groups = $this->storage->loadMultiple();
    if (count($all_groups) > 1) {
      // Add the ability link permission groups to the top of the list of
      // permissions.
      $form['permissions']['permission_group.linked_groups'] = [
        [
          '#wrapper_attributes' => [
            'colspan' => count($groups) + 1,
            'class' => ['module'],
            'id' => 'module-permission_group-linked_groups',
          ],
          '#markup' => $this->t('Linked permission groups'),
        ],
      ];
      foreach ($all_groups as $all_group_id => $all_group) {
        // Fill in default values for the permission.
        $perm_item = [
          'title' => $all_group->label(),
          'description' => $all_group->description(),
          'restrict access' => $all_group->restrictAccess($permissions),
        ];
        $perm_item['warning'] = !empty($perm_item['restrict access']) ? $this->t('Warning: this permission group has security implications.') : '';

        $form['permissions'][$all_group_id]['description'] = [
          '#type' => 'inline_template',
          '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
          '#context' => [
            'title' => $perm_item['title'],
          ],
        ];
        // Show the permission description.
        if (!$hide_descriptions) {
          $form['permissions'][$all_group_id]['description']['#context']['description'] = $perm_item['description'];
          $form['permissions'][$all_group_id]['description']['#context']['warning'] = $perm_item['warning'];
        }
        foreach ($groups as $group_id => $group) {
          $form['permissions'][$all_group_id][$group_id] = [
            '#title' => $group->label() . ': ' . $perm_item['title'],
            '#title_display' => 'invisible',
            '#wrapper_attributes' => [
              'class' => ['checkbox'],
            ],
            '#type' => 'checkbox',
            '#default_value' => in_array($all_group_id, $group->getPermissionGroups()) ? 1 : 0,
            '#attributes' => [
              'class' => [
                'rid-' . $group_id,
                'js-rid-' . $group_id,
              ],
            ],
            '#parents' => [static::LINKED_GROUP_PARENT, $group_id, $all_group_id],
            // Disable if the permission is for the group.
            '#disabled' => $all_group_id === $group->id(),
          ];
        }
      }
    }

    // @todo disable or somehow mark permissions that are in linked groups? This
    //   is tricky.
    $permissions_by_provider = [];
    foreach ($permissions as $permission_name => $permission) {
      $permissions_by_provider[$permission['provider']][$permission_name] = $permission;
    }

    // Move the access content permission to the Node module if it is installed.
    if ($this->moduleHandler->moduleExists('node')) {
      // Insert 'access content' before the 'view own unpublished content' key
      // in order to maintain the UI even though the permission is provided by
      // the system module.
      $keys = array_keys($permissions_by_provider['node']);
      $offset = (int) array_search('view own unpublished content', $keys);
      $permissions_by_provider['node'] = array_merge(
        array_slice($permissions_by_provider['node'], 0, $offset),
        ['access content' => $permissions_by_provider['system']['access content']],
        array_slice($permissions_by_provider['node'], $offset)
      );
      unset($permissions_by_provider['system']['access content']);
    }

    foreach ($permissions_by_provider as $provider => $permissions) {
      // Module name.
      $form['permissions'][$provider] = [
        [
          '#wrapper_attributes' => [
            'colspan' => count($groups) + 1,
            'class' => ['module'],
            'id' => 'module-' . $provider,
          ],
          '#markup' => $this->moduleHandler->getName($provider),
        ],
      ];
      foreach ($permissions as $perm => $perm_item) {
        // Fill in default values for the permission.
        $perm_item += [
          'description' => '',
          'restrict access' => FALSE,
          'warning' => !empty($perm_item['restrict access']) ? $this->t('Warning: this permission has security implications.') : '',
        ];
        $form['permissions'][$perm]['description'] = [
          '#type' => 'inline_template',
          '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
          '#context' => [
            'title' => $perm_item['title'],
          ],
        ];
        // Show the permission description.
        if (!$hide_descriptions) {
          $form['permissions'][$perm]['description']['#context']['description'] = $perm_item['description'];
          $form['permissions'][$perm]['description']['#context']['warning'] = $perm_item['warning'];
        }
        foreach ($groups as $group_id => $group) {
          $form['permissions'][$perm][$group_id] = [
            '#title' => $group->label() . ': ' . $perm_item['title'],
            '#title_display' => 'invisible',
            '#wrapper_attributes' => [
              'class' => ['checkbox'],
            ],
            '#type' => 'checkbox',
            '#default_value' => in_array($perm, $group->permissions()) ? 1 : 0,
            '#attributes' => ['class' => ['rid-' . $group_id, 'js-rid-' . $group_id]],
            '#parents' => [$group_id, $perm],
          ];
        }
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'user/drupal.user.permissions';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('group_ids') as $group_id) {
      $permissions = array_keys(array_filter((array) $form_state->getValue($group_id)));
      $linked_groups = array_keys(array_filter((array) $form_state->getValue([static::LINKED_GROUP_PARENT, $group_id])));
      /** @var \Drupal\permission_group\Entity\PermissionGroup $group */
      $group = $this->storage->load($group_id);
      $group
        ->setPermissions($permissions)
        ->setPermissionGroups($linked_groups)
        ->save();
    }

    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

}
