<?php

namespace Drupal\permission_group\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Customised the description to alert the user to the affects on roles.
 *
 * @internal
 *   Remove once third parties can add more granular dependencies than just
 *   module dependencies.
 */
class PermissionGroupDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $affected_roles = [];
    foreach (Role::loadMultiple() as $role) {
      if (in_array($this->entity->id(), $role->getThirdPartySetting('permission_group', 'groups', []), TRUE)) {
        $affected_roles[] = $role->label();
      }
    }
    if (!empty($affected_roles)) {
      $form['description'] = [
        '#type' => 'inline_template',
        '#template' => '<p>{% trans %}This action will remove the permission group and related permissions from the following roles:{% endtrans %}</p>{{ affected_roles }}<p>{% trans %}The action cannot be undone.{% endtrans %}</p>',
        '#context' => [
          'affected_roles' => [
            '#theme' => 'item_list',
            '#items' => $affected_roles,
          ],
        ],
      ];
    }

    return $form;
  }

}
