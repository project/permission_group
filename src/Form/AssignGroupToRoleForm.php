<?php

namespace Drupal\permission_group\Form;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\permission_group\Entity\PermissionGroup;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to assign permission groups to roles.
 *
 * @internal
 */
class AssignGroupToRoleForm extends FormBase {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * The permission group storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupStorage;

  /**
   * Constructs a new AssignGroupToRoleForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $group_storage
   *   The permission group storage.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, RoleStorageInterface $role_storage, ConfigEntityStorageInterface $group_storage) {
    $this->permissionHandler = $permission_handler;
    $this->roleStorage = $role_storage;
    $this->groupStorage = $group_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('entity_type.manager')->getStorage('permission_group')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permission_group_assign_group_to_role';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Remove admin roles.
    /** @var \Drupal\user\Entity\Role[] $roles */
    $roles = array_filter($this->roleStorage->loadMultiple(), function (Role $role) {
      return !$role->isAdmin();
    });

    /** @var \Drupal\permission_group\Entity\PermissionGroup[] $groups */
    $groups = $this->groupStorage->loadMultiple();

    if (empty($groups)) {
      $form['empty_message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('There are no permission groups available.'),
      ];
      return $form;
    }

    // Store $role_names for use when saving the data.
    $form['role_names'] = [
      '#type' => 'value',
      '#value' => array_keys($roles),
    ];
    // Render role/permission overview:
    $hide_descriptions = system_admin_compact_mode();

    $form['system_compact_link'] = [
      '#id' => FALSE,
      '#type' => 'system_compact_link',
    ];

    $form['permission_groups'] = [
      '#type' => 'table',
      '#header' => [$this->t('Permission Groups')],
      '#id' => 'permission_groups',
      '#sticky' => TRUE,
    ];
    foreach ($roles as $role) {
      $form['permission_groups']['#header'][] = [
        'data' => $role->label(),
        'class' => ['checkbox'],
      ];
    }

    $permission_list = $this->permissionHandler->getPermissions();

    foreach ($groups as $group) {
      $form['permission_groups'][$group->id()]['description'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
        '#context' => [
          'title' => $group->label(),
        ],
      ];
      // Show the permission description.
      if (!$hide_descriptions) {
        $form['permission_groups'][$group->id()]['description']['#context']['description'] = $group->description();
        if ($group->restrictAccess($permission_list)) {
          $form['permission_groups'][$group->id()]['description']['#context']['warning'] = $this->t('Warning: Give to trusted roles only; this permission group has security implications.');
        }
      }
      foreach ($roles as $rid => $role) {
        $role_groups = $role->getThirdPartySetting('permission_group', 'groups', []);
        $form['permission_groups'][$group->id()][$rid] = [
          '#title' => $role->label() . ': ' . $group->label(),
          '#title_display' => 'invisible',
          '#wrapper_attributes' => [
            'class' => ['checkbox'],
          ],
          '#type' => 'checkbox',
          '#default_value' => (int) in_array($group->id(), $role_groups, TRUE),
          '#parents' => [$rid, $group->id()],
        ];
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permission group assignments'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\Entity\Role[] $roles */
    $roles = $this->roleStorage->loadMultiple($form_state->getValue('role_names'));
    foreach ($roles as $role_id => $role) {
      $group_ids = array_keys(array_filter((array) $form_state->getValue($role_id)));
      PermissionGroup::roleSave($role, $group_ids);
    }

    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

}
