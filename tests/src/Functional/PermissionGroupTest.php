<?php

namespace Drupal\Tests\permission_group\Functional;

use Drupal\permission_group\Entity\PermissionGroup;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the Permission Group module.
 *
 * @group permission_group
 */
class PermissionGroupTest extends BrowserTestBase {

  /**
   * User with admin privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * Enable some core module to have a few permissions available.
   *
   * @var string[]
   */
  protected static $modules = ['block', 'node', 'permission_group'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer permissions',
      'administer users',
    ]);
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Tests the permission group module.
   */
  public function testPermissionGroups() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/people');
    $this->clickLink('Permission groups');
    $this->clickLink('Add permission group');
    $this->submitForm([
      'label' => 'Test permission group',
      'description' => 'A description of the group.',
      'id' => 'test_permission_group',
    ], 'Save');
    $assert_session->pageTextContains('Created new permission group Test permission group.');
    $this->clickLink('Edit permissions');

    $page->checkField('test_permission_group[administer blocks]');
    $page->checkField('test_permission_group[administer filters]');
    $page->pressButton('Save permissions');
    $assert_session->pageTextContains('The changes have been saved.');
    $assert_session->checkboxChecked('test_permission_group[administer blocks]');
    $assert_session->checkboxChecked('test_permission_group[administer filters]');
    $assert_session->checkboxNotChecked('test_permission_group[access content overview]');

    // Test that assigning a group to a role updates the permissions.
    $role = Role::load($this->drupalCreateRole([]));
    $this->drupalGet($role->toUrl('edit-permissions-form'));
    // Ensure that the permission group has the restrict access text because it
    // contains 'administer filters'.
    $assert_session->elementTextContains('css', 'tr[data-drupal-selector=\'edit-permissions-test-permission-group\']', 'Warning: this permission group has security implications. A description of the group.');
    $assert_session->pageTextContains('Test permission group');
    $assert_session->pageTextContains('A description of the group.');
    $assert_session->checkboxNotChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxNotChecked($role->id() . '[administer filters]');
    $assert_session->checkboxNotChecked($role->id() . '[access content]');
    $page->checkField('linked_group$[' . $role->id() . '][test_permission_group]');
    // Assign a permission that is part of a group.
    $page->checkField($role->id() . '[access content]');
    $page->pressButton('Save permissions');
    $assert_session->checkboxChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxChecked($role->id() . '[administer filters]');
    $assert_session->checkboxChecked($role->id() . '[access content]');
    // Permissions managed by the permission group should be disabled.
    $assert_session->fieldDisabled($role->id() . '[administer blocks]');
    $field = $assert_session->fieldDisabled($role->id() . '[administer filters]');
    // Ensure the permission title details why it is disabled.
    $this->assertSame('This permission is part of the Test permission group permission group', $field->getAttribute('title'));
    $page->uncheckField('linked_group$[' . $role->id() . '][test_permission_group]');
    $page->pressButton('Save permissions');
    $assert_session->checkboxNotChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxNotChecked($role->id() . '[administer filters]');
    $assert_session->checkboxChecked($role->id() . '[access content]');
    $assert_session->fieldEnabled($role->id() . '[administer blocks]');
    $assert_session->fieldEnabled($role->id() . '[administer filters]');
    $page->checkField('linked_group$[' . $role->id() . '][test_permission_group]');
    $page->pressButton('Save permissions');
    $assert_session->checkboxChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxChecked($role->id() . '[administer filters]');
    $assert_session->checkboxNotChecked($role->id() . '[view own unpublished content]');
    $assert_session->checkboxChecked($role->id() . '[access content]');

    // Test that change permissions on a group updates the role.
    $permission_group = PermissionGroup::load('test_permission_group');
    $this->drupalGet($permission_group->toUrl('edit-permissions-form'));
    $page->uncheckField('test_permission_group[administer filters]');
    $page->checkField('test_permission_group[view own unpublished content]');
    $page->pressButton('Save permissions');
    $this->drupalGet($role->toUrl('edit-permissions-form'));
    $assert_session->checkboxChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxNotChecked($role->id() . '[administer filters]');
    $assert_session->checkboxChecked($role->id() . '[view own unpublished content]');
    $assert_session->checkboxChecked($role->id() . '[access content]');
    // Ensure that the permission group does not have the restrict access text
    // because it no longer contains 'administer filters'.
    $assert_session->elementTextNotContains('css', 'tr[data-drupal-selector=\'edit-permissions-test-permission-group\']', 'Warning: this permission group has security implications. A description of the group.');

    // Create another group and assign it to the same role.
    $this->drupalGet('admin/people/permission_groups/add');
    $this->submitForm([
      'label' => 'Test permission group 2',
      'description' => 'Another description of the group.',
      'id' => 'test_permission_group_2',
    ], 'Save');
    $assert_session->pageTextContains('Created new permission group Test permission group 2.');
    $this->clickLink('Edit permissions', 1);
    $page->checkField('test_permission_group_2[view own unpublished content]');
    $page->pressButton('Save permissions');
    $this->drupalGet($role->toUrl('edit-permissions-form'));
    $page->checkField('linked_group$[' . $role->id() . '][test_permission_group_2]');
    $page->pressButton('Save permissions');
    // Ensure that titles say which groups permissions are in.
    $field = $assert_session->fieldDisabled($role->id() . '[view own unpublished content]');
    $this->assertSame('This permission is part of the permission groups: Test permission group, Test permission group 2', $field->getAttribute('title'));
    $field = $assert_session->fieldDisabled($role->id() . '[administer blocks]');
    // Ensure the permission title details why it is disabled.
    $this->assertSame('This permission is part of the Test permission group permission group', $field->getAttribute('title'));

    // Test deleting a permission group that is assigned to a role.
    $this->drupalGet('admin/people');
    $this->clickLink('Permission groups');
    $this->clickLink('Delete');
    $assert_session->pageTextContains('This action will remove the permission group and related permissions from the following roles:');
    $assert_session->pageTextContains($role->label());
    $page->pressButton('Delete');
    $assert_session->pageTextContains('The permission group Test permission group has been deleted.');
    $this->drupalGet($role->toUrl('edit-permissions-form'));
    $assert_session->checkboxNotChecked($role->id() . '[administer blocks]');
    $assert_session->checkboxNotChecked($role->id() . '[administer filters]');
    $assert_session->checkboxChecked($role->id() . '[view own unpublished content]');
    $assert_session->checkboxChecked($role->id() . '[access content]');

    // Ensure that saving the permissions form does not add dependencies.
    $another_role = Role::load($this->drupalCreateRole([], NULL, 'another role'));
    $this->drupalGet('admin/people/permissions');
    $this->submitForm([], 'Save permissions');
    $another_role = $this->reloadRole($another_role->id());
    $this->assertSame([], $another_role->getDependencies());

    // Ensure that the original role is unaffected.
    $role = $this->reloadRole($role->id());
    $this->assertContains('permission_group', $role->getDependencies()['module']);
    $this->assertSame(['test_permission_group_2'], $role->getThirdPartySetting('permission_group', 'groups'));
  }

  /**
   * Tests the assign group to role form.
   */
  public function testAssignGroupToRoleForm() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $role = Role::load($this->drupalCreateRole([], NULL, 'test role'));
    // Create a role to ensure it doesn't get a dependency on the
    // permission_group module.
    $another_role = Role::load($this->drupalCreateRole([], NULL, 'another role'));
    $admin_role = Role::load($this->createAdminRole(NULL, 'test admin role'));

    $this->drupalGet('admin/people/permission_groups/assign_to_role');
    $assert_session->statusCodeEquals(403);

    $this->drupalLogin($this->drupalCreateUser([
      'assign permission group to role',
    ]));
    $this->drupalGet('admin/people/permission_groups/assign_to_role');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('There are no permission groups available.');
    $assert_session->pageTextNotContains($role->label());

    /** @var \Drupal\permission_group\Entity\PermissionGroup $group_a */
    $group_a = PermissionGroup::create(['id' => 'test_a', 'label' => 'Test Group A']);
    $group_a->setPermissions(['access content', 'administer blocks']);
    $group_a->save();

    /** @var \Drupal\permission_group\Entity\PermissionGroup $group_b */
    $group_b = PermissionGroup::create(['id' => 'test_b', 'label' => 'Test Group B']);
    $group_b->setPermissions(['access content', 'administer filters']);
    $group_b->save();

    $this->drupalGet('admin/people/permission_groups/assign_to_role');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextNotContains('There are no permission groups available.');
    $assert_session->pageTextContains($role->label());
    $assert_session->pageTextNotContains($admin_role->label());
    $assert_session->pageTextContains($group_a->label());
    $assert_session->pageTextContains($group_b->label());
    $page->checkField($role->id() . '[' . $group_a->id() . ']');
    $page->pressButton('Save permission group assignments');
    $role = $this->reloadRole($role->id());
    $this->assertTrue($role->hasPermission('access content'));
    $this->assertTrue($role->hasPermission('administer blocks'));

    $page->uncheckField($role->id() . '[' . $group_a->id() . ']');
    $page->checkField($role->id() . '[' . $group_b->id() . ']');
    $page->pressButton('Save permission group assignments');
    $role = $this->reloadRole($role->id());
    $this->assertTrue($role->hasPermission('access content'));
    $this->assertFalse($role->hasPermission('administer blocks'));
    $this->assertTrue($role->hasPermission('administer filters'));

    $page->checkField($role->id() . '[' . $group_a->id() . ']');
    $page->pressButton('Save permission group assignments');
    $role = $this->reloadRole($role->id());
    $this->assertTrue($role->hasPermission('access content'));
    $this->assertTrue($role->hasPermission('administer filters'));
    $this->assertTrue($role->hasPermission('administer blocks'));

    $page->uncheckField($role->id() . '[' . $group_a->id() . ']');
    $page->uncheckField($role->id() . '[' . $group_b->id() . ']');
    $page->pressButton('Save permission group assignments');
    $role = $this->reloadRole($role->id());
    $this->assertFalse($role->hasPermission('access content'));
    $this->assertFalse($role->hasPermission('administer filters'));
    $this->assertFalse($role->hasPermission('administer blocks'));

    $another_role = $this->reloadRole($another_role->id());
    $this->assertSame([], $another_role->getDependencies());
  }

  /**
   * Reloads a role fresh from the database.
   *
   * @param string $rid
   *   The role ID to load.
   *
   * @return \Drupal\user\Entity\Role
   *   The refreshed role.
   */
  private function reloadRole(string $rid) : Role {
    $this->refreshVariables();
    \Drupal::entityTypeManager()->getStorage('user_role')->resetCache([$rid]);
    return Role::load($rid);
  }

}
