<?php

namespace Drupal\Tests\permission_group\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the Permission Group module doesn't cause unlimited recursion.
 *
 * @group permission_group
 */
class PermissionGroupPostUpdateTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'permission_group', 'system'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
    include_once __DIR__ . '/../../../permission_group.post_update.php';
  }

  /**
   * Tests permission_group_post_update_fix_dependencies().
   *
   * @see permission_group_post_update_fix_dependencies()
   */
  public function testFixDependencies() {
    $role1 = Role::create(['id' => 'test1', 'label' => 'Test 1']);
    $role1->setThirdPartySetting('permission_group', 'groups', []);
    $role1->save();
    $this->assertContains('permission_group', $role1->getDependencies()['module']);

    $role2 = Role::create(['id' => 'test2', 'label' => 'Test 2']);
    $role2->setThirdPartySetting('permission_group', 'groups', ['does_not_matter']);
    $role2->save();
    $this->assertContains('permission_group', $role2->getDependencies()['module']);

    // Run the post update.
    $sandbox = [];
    permission_group_post_update_fix_dependencies($sandbox);

    // Reload the roles.
    $role1 = Role::load($role1->id());
    $role2 = Role::load($role2->id());

    // Test the dependency is fixed.
    $this->assertEmpty($role1->getDependencies());

    // Test the dependency remains unchanged.
    $this->assertContains('permission_group', $role2->getDependencies()['module']);
  }

}
