<?php

namespace Drupal\Tests\permission_group\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\permission_group\Entity\PermissionGroup;

/**
 * Tests the Permission Group entity's dependency management.
 *
 * @group permission_group
 */
class PermissionGroupDependenciesTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'permission_group', 'permission_group_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
  }

  /**
   * Tests dependencies.
   */
  public function testDependencies() {
    $group = PermissionGroup::create(['id' => 'a']);
    $group->setPermissions(['perm1', 'perm2']);
    $group->save();

    $this->assertSame(['module' => ['permission_group_test']], $group->getDependencies());

    $group->setPermissions(['perm1', 'perm2', 'view user email addresses']);
    $group->save();
    $this->assertSame(['module' => ['permission_group_test', 'user']], $group->getDependencies());
    $this->container->get('module_installer')->uninstall(['permission_group_test']);
    $group = PermissionGroup::load($group->id());
    $this->assertSame(['module' => ['user']], $group->getDependencies());
    $this->assertSame(['view user email addresses'], $group->permissions());
  }

}
