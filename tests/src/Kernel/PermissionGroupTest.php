<?php

namespace Drupal\Tests\permission_group\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\permission_group\Entity\PermissionGroup;
use Drupal\user\Entity\Role;

/**
 * Tests the Permission Group module.
 *
 * @group permission_group
 */
class PermissionGroupTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'user', 'permission_group', 'permission_group_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * Tests recursive groups.
   */
  public function testRecursion() {
    $group_a = PermissionGroup::create(['id' => 'a']);
    $group_a->setPermissions(['perm1', 'perm2']);
    $group_a->save();

    $group_b = PermissionGroup::create(['id' => 'b']);
    $group_b->setPermissions(['perm1', 'perm3']);
    $group_b->setPermissionGroups(['a']);
    $group_b->save();

    $group_a->setPermissionGroups(['b'])->save();

    $this->assertSame(['perm1', 'perm2'], $group_a->permissions());
    $this->assertSame(['perm1', 'perm2', 'perm3'], $group_a->permissions(TRUE));

    // A longer chain of groups.
    $group_c = PermissionGroup::create(['id' => 'c']);
    $group_c->setPermissions(['perm2', 'perm4']);
    $group_c->setPermissionGroups(['a']);
    $group_c->save();

    $this->assertSame(['perm1', 'perm2', 'perm3', 'perm4'], $group_c->permissions(TRUE));
    $this->assertFalse($group_a->restrictAccess());
    $this->assertFalse($group_b->restrictAccess());
    $this->assertFalse($group_c->restrictAccess());

    // Add a restricted access permission.
    $group_a->setPermissions(['perm1', 'perm2', 'administer permissions']);
    $group_a->save();
    $this->assertSame(['administer permissions', 'perm1', 'perm2', 'perm3', 'perm4'], $group_c->permissions(TRUE));
    $this->assertTrue($group_a->restrictAccess());
    $this->assertTrue($group_b->restrictAccess());
    $this->assertTrue($group_c->restrictAccess());

    // Break the loop.
    $group_b->setPermissionGroups([]);
    $group_b->save();
    $this->assertTrue($group_a->restrictAccess());
    $this->assertFalse($group_b->restrictAccess());
    $this->assertTrue($group_c->restrictAccess());
  }

  /**
   * Tests roles with linked groups.
   */
  public function testChangePermisionsWithLinkedGroups() {
    $group_a = PermissionGroup::create(['id' => 'a']);
    $group_a->setPermissions(['perm1', 'perm2']);
    $group_a->save();

    $group_b = PermissionGroup::create(['id' => 'b']);
    $group_b->setPermissions(['perm1', 'perm3']);
    $group_b->setPermissionGroups(['a']);
    $group_b->save();

    $group_c = PermissionGroup::create(['id' => 'c']);
    $group_c->setPermissionGroups(['b']);
    $group_c->save();

    $role = Role::create(['id' => 'test', 'label' => 'Test']);
    $role->setThirdPartySetting('permission_group', 'groups', ['c']);
    $role->save();

    $this->assertSame(['perm1', 'perm2', 'perm3'], $role->getPermissions());

    $group_a->setPermissions(['perm4']);
    $group_a->save();

    $storage = $this->container->get('entity_type.manager')->getStorage('user_role');
    $storage->resetCache();
    $role = Role::load($role->id());
    // @todo Remove sorting when
    //   https://www.drupal.org/project/drupal/issues/3039499 is fixed.
    $permissions = $role->getPermissions();
    sort($permissions);
    $this->assertSame(['perm1', 'perm3', 'perm4'], $permissions);

    $group_b->setPermissions(['perm3']);
    $group_b->setPermissionGroups([]);
    $group_b->save();

    $storage->resetCache();
    $role = Role::load($role->id());
    $this->assertSame(['perm3'], $role->getPermissions());
  }

  /**
   * Tests roles with linked groups.
   */
  public function testDeleteWithLinkedGroups() {
    $group_a = PermissionGroup::create(['id' => 'a']);
    $group_a->setPermissions(['perm1', 'perm2']);
    $group_a->save();

    $group_b = PermissionGroup::create(['id' => 'b']);
    $group_b->setPermissions(['perm1', 'perm3']);
    $group_b->setPermissionGroups(['a']);
    $group_b->save();

    $group_c = PermissionGroup::create(['id' => 'c']);
    $group_c->setPermissionGroups(['b']);
    $group_c->save();

    $role = Role::create(['id' => 'test', 'label' => 'Test']);
    $role->setThirdPartySetting('permission_group', 'groups', ['c']);
    $role->save();

    $this->assertSame(['perm1', 'perm2', 'perm3'], $role->getPermissions());

    $group_a->delete();

    $storage = $this->container->get('entity_type.manager')->getStorage('user_role');
    $storage->resetCache();
    $role = Role::load($role->id());
    // @todo Remove sorting when
    //   https://www.drupal.org/project/drupal/issues/3039499 is fixed.
    $permissions = $role->getPermissions();
    sort($permissions);
    $this->assertSame(['perm1', 'perm3'], $permissions);

    $pg_storage = $this->container->get('entity_type.manager')->getStorage('permission_group');
    $pg_storage->resetCache();
    $group_b = PermissionGroup::load($group_b->id());
    $this->assertEmpty($group_b->getPermissionGroups());

    $group_c->delete();
    $storage->resetCache();
    $role = Role::load($role->id());
    $this->assertSame([], $role->getPermissions());
  }

}
